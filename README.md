# rdz-propsim-winprop (a WinProp- and python-based propagation simulation tool for POWDER-RDZ)

The [POWDER-RDZ portal](https://rdz.powderwireless.net) is a prototype
automatic spectrum-sharing management system for the POWDER testbed in Salt
Lake City, Utah, which is part of the NSF Platforms for Advanced Wireless
Research program. The portal provides mechanisms to share electromagnetic
(radio-frequency) spectrum between experimental or test systems and existing
spectrum users, and between multiple experimental systems.

This repository contains a WinProp- and python-based propagation simulation
tool, and a gRPC service that implements the POWDER-RDZ `propsim` API.


## Quick dev start

You can easily run this service in standalone mode (e.g. without the RDZ
identity service) via `docker compose`.  (The default docker env vars
(`deploy/env/rdz-propsim-winprop-local-dev.env`) comment out the
`IDENTITY_RPC_ENDPOINT` var, and if unset, this service does not attempt to
register itself with an RDZ Identity server.)

    docker compose -f deploy/docker-compose.yml up

Grab the service IP address:

    export SIP=`docker inspect rdz-propsim-winprop-local-dev -f '{{ index . "NetworkSettings" "Networks" "rdz-service-local-dev-net" "IPAddress"}}'`
    echo $SIP

Initial testing with your service is easiest via the `grpcurl` tool.  Install this via

    go install github.com/fullstorydev/grpcurl/cmd/grpcurl@latest

(This should leave a binary in `~/go/bin/grpcurl`.)

Unfortunately, you will need not only the RDZ gRPC proto files for this tool
to introspect over, but you'll also need the standard `validate.proto`
validation protobuf defs.  If you don't have this on your system already, you can obtain it via:

    pushd /tmp && wget https://github.com/bufbuild/protoc-gen-validate/raw/main/validate/validate.proto && popd

Next, clone the RDZ API repo.  (If you have specific version or branch
requirements, be sure to checkout the right refspec.)

    pushd /tmp && git clone https://gitlab.flux.utah.edu/powder-rdz/rdz-api && popd

Finally, you can run a simple test job sequence:

    export JOB_ID=`uuidgen`
    ~/go/bin/grpcurl -plaintext \
        -import-path /tmp/rdz-api/proto/rdz/identity/v1 \
        -import-path /tmp/rdz-api/proto/rdz/propsim/v1 \
        -import-path /tmp/ \
        -proto propsim.proto \
        -d '{"job":{"id":"'$JOB_ID'","parameters":[{"name":"radio_name","s":"ustar"}]}}' $SIP:8055 propsim.v1.propsim.JobService/CreateJob

which should return something like

```
{
  "jobId": "02980890-5d7b-4f2d-8038-bc30637be14c"
}
```

Next, start the job.

    ~/go/bin/grpcurl -plaintext \
        -import-path /tmp/rdz-api/proto/rdz/identity/v1 \
        -import-path /tmp/rdz-api/proto/rdz/propsim/v1 \
        -import-path /tmp/ \
        -proto propsim.proto \
        -d '{"job_id":"'$JOB_ID'"}' $SIP:8055 propsim.v1.propsim.JobService/StartJob

Finally, watch for the job to complete.  This will return a stream of events
until the job completes.  At that point, the stream will terminate on the
server side.

    ~/go/bin/grpcurl -plaintext \
        -import-path /tmp/rdz-api/proto/rdz/identity/v1 \
        -import-path /tmp/rdz-api/proto/rdz/propsim/v1 \
        -import-path /tmp/ \
        -proto propsim.proto \
        -d '{"job_id":"'$JOB_ID'"}' $SIP:8055 propsim.v1.propsim.JobService/GetJobEvents

```
{
  "job": {
    "id": "02980890-5d7b-4f2d-8038-bc30637be14c",
    "parameters": [
      {
        "name": "radio_name",
        "s": "ustar"
      }
    ],
    "status": "JOB_STATUS_RUNNING",
    "completionPercentage": 0.2,
    "startedAt": "2023-11-13T04:42:58.746612Z",
    "updatedAt": "2023-11-13T04:42:59.748553Z"
  }
}
{
  "event": "JOB_EVENT_PROGRESS",
  "job": {
    "id": "02980890-5d7b-4f2d-8038-bc30637be14c",
    "parameters": [
      {
        "name": "radio_name",
        "s": "ustar"
      }
    ],
    "status": "JOB_STATUS_RUNNING",
    "completionPercentage": 0.4,
    "startedAt": "2023-11-13T04:42:58.746612Z",
    "updatedAt": "2023-11-13T04:43:00.749936Z"
  }
}
{
  "event": "JOB_EVENT_PROGRESS",
  "job": {
    "id": "02980890-5d7b-4f2d-8038-bc30637be14c",
    "parameters": [
      {
        "name": "radio_name",
        "s": "ustar"
      }
    ],
    "status": "JOB_STATUS_RUNNING",
    "completionPercentage": 0.6,
    "startedAt": "2023-11-13T04:42:58.746612Z",
    "updatedAt": "2023-11-13T04:43:01.751770Z"
  }
}
{
  "event": "JOB_EVENT_PROGRESS",
  "job": {
    "id": "02980890-5d7b-4f2d-8038-bc30637be14c",
    "parameters": [
      {
        "name": "radio_name",
        "s": "ustar"
      }
    ],
    "status": "JOB_STATUS_RUNNING",
    "completionPercentage": 0.8,
    "startedAt": "2023-11-13T04:42:58.746612Z",
    "updatedAt": "2023-11-13T04:43:02.752739Z"
  }
}
{
  "event": "JOB_EVENT_PROGRESS",
  "job": {
    "id": "02980890-5d7b-4f2d-8038-bc30637be14c",
    "parameters": [
      {
        "name": "radio_name",
        "s": "ustar"
      }
    ],
    "status": "JOB_STATUS_RUNNING",
    "completionPercentage": 1,
    "startedAt": "2023-11-13T04:42:58.746612Z",
    "updatedAt": "2023-11-13T04:43:03.753765Z"
  }
}
{
  "job": {
    "id": "02980890-5d7b-4f2d-8038-bc30637be14c",
    "parameters": [
      {
        "name": "radio_name",
        "s": "ustar"
      }
    ],
    "status": "JOB_STATUS_COMPLETE",
    "completionPercentage": 1,
    "startedAt": "2023-11-13T04:42:58.746612Z",
    "updatedAt": "2023-11-13T04:43:03.753765Z",
    "finishedAt": "2023-11-13T04:43:03.754155Z"
  }
}
```

Finally, delete the job.

    ~/go/bin/grpcurl -plaintext \
        -import-path /tmp/rdz-api/proto/rdz/identity/v1 \
        -import-path /tmp/rdz-api/proto/rdz/propsim/v1 \
        -import-path /tmp/ \
        -proto propsim.proto \
        -d '{"job_id":"'$JOB_ID'"}' $SIP:8055 propsim.v1.propsim.JobService/DeleteJob

(NB: if you had started another `GetJobEvents` stream, you would have seen a
brief notification of the job becoming `JOB_STATUS_DELETED`, and that stream
would have terminated as well.)
