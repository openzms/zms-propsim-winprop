#!/usr/bin/env python3

import asyncio
import sys
import os
import logging

from rdz_propsim_winprop.server.grpc.server import PropSimServer
from rdz_propsim_winprop.server.config import Config
from rdz_propsim_winprop.util.log import configure_logging

async def main():
    if os.getenv("CONFIG_DEBUG", False):
        configure_logging(debug=True)
    if not os.getenv("GRPC_DEBUG", False):
        logging.getLogger('hpack.hpack').setLevel(logging.WARN)
    config = Config()
    config.load(argv=sys.argv[1:] if len(sys.argv) > 1 else [])
    configure_logging(debug=config.debug)
    server = PropSimServer(config)
    await server.run()

if __name__ == "__main__":
    asyncio.run(main())
