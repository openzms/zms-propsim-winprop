
import logging
import time

LOG = logging.getLogger(__name__)

def run_winprop_cli(params, output_dir=None, server_job_state=None):
    LOG.debug("would have run winprop CLI client; faking via sleep")
    for i in range(1, 6):
        time.sleep(1.0)
        if server_job_state:
            server_job_state.update_completion_sync(i/5.0)
