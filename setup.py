#!/usr/bin/env python3

import setuptools

if __name__ == "__main__":
    setuptools.setup(
        name="rdz-propsim-winprop",
        version="0.1.0",
        author="Miguel Angel-Gomez, David M. Johnson",
        author_email="",
        maintainer="David M. Johnson",
        maintainer_email="johnsond@flux.utah.edu"
        url="https://gitlab.flux.utah.edu/powder-rdz/rdz-propsim-winprop",
        description="A WinProp-based propagation service for POWDER-RDZ.",
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Environment :: Other Environment",
            "Intended Audience :: Developers",
            "Operating System :: OS Independent",
            "Programming Language :: Python",
            "Programming Language :: Python :: 3",
            "Topic :: Utilities",
        ],
        packages=setuptools.find_packages(),
        install_requires=[
            "powder-rdz-api @ git+https://gitlab.flux.utah.edu/powder-rdz/rdz-api.git@7b673de#subdirectory=python",
        ],
        python_requires=">=3.7",
        setup_requires=[
            "setuptools",
        ],
    )
