
class PropSimWinpropError(Exception):
    pass

class ConfigError(PropSimWinpropError):
    pass

class JobExistsError(PropSimWinpropError):
    pass

class InvalidJobError(PropSimWinpropError):
    pass

class InvalidJobOutputError(PropSimWinpropError):
    pass

class JobParameterError(PropSimWinpropError):
    pass
