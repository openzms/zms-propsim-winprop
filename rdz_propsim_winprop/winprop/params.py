class WinpropParams:
    """Represents WinProp input parameters"""

    def __init__(self, radio_name, *args, **kwargs):
        """
        :param radio_name: basestation endpoint name e.g. "cbrssdr1-ustar-comp"
        """

        self.radio_name = radio_name
